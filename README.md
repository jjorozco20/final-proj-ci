---
title: "CI Final Project"
tags: ""
---

---

# CI Final Project

```By: José Juan Orozco Serrano```

---

The goal of the final project is to demonstrate all the knowledge acquired in the course using an example source code repository where you will need to set up the CI pipeline in GitLab and leverage the following stages at a minimum:

* Running **unit tests** as part of the CI Pipeline
* Create a **Quality Gate** with *SonarQube*
* Storing **artifacts** with *JFrog Artifactory*
* Integrating **DevSecOps** in the pipeline with *Snyk.io*

---

### Project configuration

In the ```.gitlab-ci.yml``` file we need to put the following data:

```
image: maven:3.6.3-jdk-11

stages:
  - test
  - quality-gate
  - artifactory
  - devsecops

```

Then, you need to specify the stages based on the list's order.

---

### Unit tests

The pipeline  is  expected  to  run  unit  tests  successfully,  this  can  be  done  running “mvn  test” with  maven.

```

test:
  stage: test
  tags:
    - docker

  script:
    - echo "Maven test started"
    - mvn test

```

After that configuration, Maven will run all the project's tests and a Job named Test will be started.

![Test Job](https://boostnote.io/api/teams/0hn8TvE4U/files/1e892fab24cb69ae633425fe6a4e1c8bb8e2fc0abcd8bccd406cb351f22185c2-image.png)

---

### Quality Gate

*Sonarqube* defines: Quality Gates tell you at every analysis whether your code is ready to release. For this example we use [sonarcloud.io](sonarcloud.io) as a service to analyze our project from the pipeline.

Before we start with the stage config, we need to put some environment variables at the top of the configuration; use ``` image: maven:3.6.3-jdk-11``` as a reference:

```

variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task


image: maven:3.6.3-jdk-11

```
We need to add the **Access Token** for *Sonarqube* in *GitLab* at ```Profile->Preferencies->Access Tokens``` and paste it in the interface that [sonarcloud.io](sonarcloud.io) provides to you.

![Access tokens](https://boostnote.io/api/teams/0hn8TvE4U/files/b322881a7c0fcb9c9b9da509f7297003a0bcba76c7dc35caa8fc664f98d351a7-image.png)

And also we need to add some variables in GitLab at ```Settings->CI-CD->Variables```

![image.png](https://boostnote.io/api/teams/0hn8TvE4U/files/8eb0f1d0366edc165e2a70c44e343af082907a70189280e44978321aef8ddab4-image.png)

After that, we need to update our ```pom.xml``` file with the following:

```

<properties>
  <sonar.organization>jjorozco20</sonar.organization>

```

Once that those things are properly configured, you can put the following stage in ```.gitlab-ci.yml``` file

```

sonarcloud-check:
  stage: quality-gate
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn verify sonar:sonar -Dsonar.projectKey=jjorozco20_final-proj-ci
  only:
    - merge_requests
    - master
    - develop

```

--- 

### Jfrog Artifactory

You  will  need  to use  a  *JFrog  Artifactory* self-hosted server  you  created,  or  you  may  use the *SaaS*  option [Right here](https://jfrog.com/start-free/). The pipeline will begin to store the artifacts in the **Artifactory** deployment after each run.

Before we start with the stage config, we need to grant access to *JFrog* with an **Access Token**, and then, we need to put some environment variables within```Settings->CI-CD->Variables```

![Access tokens](https://boostnote.io/api/teams/0hn8TvE4U/files/b322881a7c0fcb9c9b9da509f7297003a0bcba76c7dc35caa8fc664f98d351a7-image.png)

![JFrog Environment Vars](https://boostnote.io/api/teams/0hn8TvE4U/files/26393ffdf8c684655b1a826430827731ab703c33051ecb2088414a2c49ec7b67-image.png)

After that, we need to update our ```pom.xml``` file with the following:

```

<distributionManagement>
    <repository>
        <id>central</id>
        <name>a0samjpbp0tca-artifactory-primary-0-releases</name>
        <url>https://juanorozco.jfrog.io/artifactory/ci-final-project</url>
    </repository>
    <snapshotRepository>
        <id>snapshots</id>
        <name>a0samjpbp0tca-artifactory-primary-0-snapshots</name>
        <url>https://juanorozco.jfrog.io/artifactory/ci-final-project</url>
    </snapshotRepository>
</distributionManagement>

```

And you need to go to your *JFrog* dashboard to set up the repository in which you are going to store the artifacts; Export the configurations and push it inside an ```settings.xml``` file.

```

<?xml version="1.0" encoding="UTF-8"?>
<settings xsi:schemaLocation="https://maven.apache.org/SETTINGS/1.2.0 https://maven.apache.org/xsd/settings-1.2.0.xsd" xmlns="https://maven.apache.org/SETTINGS/1.2.0"
    xmlns:xsi="https://www.w3.org/2001/XMLSchema-instance">
  <servers>
    <server>
      <username>juan.orozco@jala-foundation.org</username>
      <password>AP2bZz2ZKjfUQrfGKEXgYhxUZ1F</password>
      <id>central</id>
    </server>
    <server>
      <username>juan.orozco@jala-foundation.org</username>
      <password>AP2bZz2ZKjfUQrfGKEXgYhxUZ1F</password>
      <id>snapshots</id>
    </server>
  </servers>
  <profiles>
    <profile>
      <repositories>
        <repository>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <id>central</id>
          <name>jala-libs-release</name>
          <url>https://juanorozco.jfrog.io/artifactory/jala-libs-release</url>
        </repository>
        <repository>
          <snapshots />
          <id>snapshots</id>
          <name>jala-libs-snapshot</name>
          <url>https://juanorozco.jfrog.io/artifactory/jala-libs-snapshot</url>
        </repository>
      </repositories>
      <pluginRepositories>
        <pluginRepository>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <id>central</id>
          <name>jala-libs-release</name>
          <url>https://juanorozco.jfrog.io/artifactory/jala-libs-release</url>
        </pluginRepository>
        <pluginRepository>
          <snapshots />
          <id>snapshots</id>
          <name>jala-libs-snapshot</name>
          <url>https://juanorozco.jfrog.io/artifactory/jala-libs-snapshot</url>
        </pluginRepository>
      </pluginRepositories>
      <id>artifactory</id>
    </profile>
  </profiles>
  <activeProfiles>
    <activeProfile>artifactory</activeProfile>
  </activeProfiles>
</settings>

```

Then, you can go ahead to configure the ```.gitlab-ci.yml``` file:

```

jfrog:
  image: maven:3.6.3-jdk-11 
  stage: artifactory

  script:
    - mvn $MAVEN_CLI_OPTS deploy -s settings.xml
  only:
    - master

```

---

### DevSecOps

This stage is for discovering vulnerabilities in the code that you have staged long ago in the different stages. You will need to create a *snyk.io* free account [right here](https://snyk.io/) and set up the Integration of *Snyk* with **GitLab API** and an integration with from the pipeline with *Snyk* via **Snyk CLI** 

Before we start with the stage config, we need to grant access to *Snyk* with an **Access Token**, and then, we need to put some environment variables within```Settings->CI-CD->Variables```

![Access tokens](https://boostnote.io/api/teams/0hn8TvE4U/files/b322881a7c0fcb9c9b9da509f7297003a0bcba76c7dc35caa8fc664f98d351a7-image.png)

![image.png](https://boostnote.io/api/teams/0hn8TvE4U/files/47389be28902dcc0798d4730d9a6da2fea90c63c8d9dc67fcf93fd44c1306a16-image.png)

After that, we need to update our ```pom.xml``` file with the following:

```

<plugin>
  <groupId>io.snyk</groupId>
  <artifactId>snyk-maven-plugin</artifactId>
  <version>1.2.6</version>
  <executions>
     <execution>
       <id>snyk-test</id>
       <phase>test</phase>
       <goals>
         <goal>test</goal>
       </goals>
     </execution>
     <execution>
       <id>snyk-monitor</id>
       <phase>install</phase>
       <goals>
        <goal>monitor</goal>
       </goals>
     </execution>
   </executions>
   <configuration>
     <apiToken>${SNYK_TOKEN}</apiToken>
     <failOnSeverity>false</failOnSeverity>
     <org>jjorozco20</org>
     <args>
       <arg>--all-projects</arg>
     </args>
   </configuration>
</plugin>

```

Then, you can go ahead to configure the ```.gitlab-ci.yml``` file:

```

snyk:
  stage: devsecops
  script:
    - mvn snyk:test
    - mvn snyk:monitor
  only:
    - master

```

And that's all, you are now covered by the Continuous Deployment. Look at this beauty:

![Best feeling ever](https://boostnote.io/api/teams/0hn8TvE4U/files/1287996709cc8e7530fbabb062b376159ef634ffb1ae7b1affdd42991d22f3e0-image.png)
